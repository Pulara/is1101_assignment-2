#include<stdio.h>
int main() {
      int fNumber, sNumber, temp;
      
      //Reading numbers
      printf("Enter first number: ");
      scanf("%d", &fNumber);
      printf("Enter second number: ");
      scanf("%d", &sNumber);

      //Calculation
      temp = fNumber;
      fNumber = sNumber;
      sNumber = temp;

      //Displaying results
      printf("Now the first number is = %d\n", fNumber);
      printf("Now the second number is = %d", sNumber);
      
      return 0;
      
}
