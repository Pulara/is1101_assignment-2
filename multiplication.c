#include <stdio.h>
int main() {
    float num1, num2, ans;

    //Reading numbers
    printf("Enter first number: ");
    scanf("%f", &num1); 
    printf("Enter second number: ");
    scanf("%f", &num2);  
 
    //Calculation
    ans = num1 * num2;

    //Displaying answer
    printf("Answer = %.2f", ans);
    
    return 0;
}