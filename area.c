#include<stdio.h>
 
int main() {
    float r, area;
    
    //Reading radius
    printf("Enter the radius of the disk : ");
    scanf("%f", &r);
    
    //Calculation
    area = 3.14 * r * r;

    //Displaying area
    printf("Area of the disk : %.2f", area);
 
    return (0);
}
